package org.name;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class Main3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/** DECLARATIONS DES COMMUNES , MAIRES & DEPARTEMENTS **/
		Commune commune1 = new Commune();			
		Commune commune2 = new Commune();	
		Commune commune3 = new Commune();
	
		Maire maire1 = new Maire();
		Maire maire2 = new Maire();
		Maire maire3 = new Maire();

		Departement departement1 = new Departement();
	
		
		
		/**AJOUTS**/
		commune1.setNomCommune("Aubervilliers");
		commune1.setCodePostal("93300");
		commune1.setMaire(maire1);
		commune1.setDepartement(departement1);
		
//		System.out.println("***********TEST COMMUNE1***********");
		
		commune2.setNomCommune("AulnaySousBois");
		commune2.setCodePostal("93600");
		commune2.setMaire(maire2);
		commune2.setDepartement(departement1);
	
		commune3.setNomCommune("Bagnolet");
		commune3.setCodePostal("93170");
		commune3.setMaire(maire3);
		commune3.setDepartement(departement1);
		
		
		/**MAIRE DE CHAQUE COMMUNES**/
		maire1.setNomMaire("Pascal Beaudet");
		maire1.setCommune(commune1);
	
		maire2.setNomMaire("BrunoBeschizza");		
		maire2.setCommune(commune2);
		
//		System.out.println("***********TEST MAIRE1***********");
		
		maire3.setNomMaire("GTony Di");		
		maire3.setCommune(commune3);
		
		departement1.setNomDepartement("Seine-Saint-Denis");  
		departement1.AddCommune(commune1);
		departement1.AddCommune(commune2);
		departement1.AddCommune(commune3);	
		

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-testExo1");
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		em.persist(departement1);
		em.getTransaction().commit();
		
		//Test des methodes
		System.out.println("Nom du departement: ");
		System.out.println(departement1.getNomDepartement().toString());
		System.out.println("***********Recuperation du maire de la commune donn�e en parametre ***********");
		System.out.println(departement1.getMaire(commune1).toString());
		System.out.println("\n***********Recuperation des maires qui admin les communes du departement: Seine-Saint-Denis ***********");
		System.out.println(departement1.getMaires().toString());
		System.out.println(departement1.getMaire(commune1).toString());
	}
}
