package org.name;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class MainBateau {

	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		Marin marin1 =  new Marin();
		marin1.setNom("Surcouf");
		marin1.setPrenom("Robert");
		marin1.setDateDeNaissance(Date.valueOf("1958-1-1"));

		Marin marin2 =  new Marin();
		marin2.setNom("Boris");
		marin2.setPrenom("VOUDOUNON");
		calendar.set(1974,2,15);
		marin2.setDateDeNaissance(calendar.getTime());

		Marin marin3 =  new Marin();
		marin3.setNom("AKenny");
		marin3.setPrenom("Rogers");
		calendar.set(1966,12,19);
		marin3.setDateDeNaissance(calendar.getTime());

		Marin marin4 =  new Marin();
		marin4.setNom("Fall");
		marin4.setPrenom("Ansou");
		calendar.set(1988,8,8);
		marin4.setDateDeNaissance(calendar.getTime());


		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-testExo1");

		EntityManager em = emf.createEntityManager();

		Bateau bateau1 = new Bateau();
		bateau1.setNom("Cargo");

		Bateau bateau2 = new Bateau();
		bateau2.setNom("Bombardier");


		bateau1.getEquipage().add(marin1);
		bateau1.getEquipage().add(marin2);
		
	
		bateau2.getEquipage().add(marin4);
		bateau2.getEquipage().add(marin3);

		bateau1.setStatut(Statut.MER);
		bateau2.setStatut(Statut.PORT);
		// Create
		em.getTransaction().begin();
		
		em.persist(bateau1);
		em.persist(bateau2);

		em.getTransaction().commit();

		System.out.println("***********************************************************************************");

		for(Marin m : bateau1.getEquipage()){
			System.out.println(m);
		}

		System.out.println("***********************************************************************************");
		for(Marin m : bateau2.getEquipage()){
			System.out.println(m);
		}


		// Retrieve

		Bateau retrievedBateau = em.find(Bateau.class, 1L);
		System.out.println("Retieved Bateau => " + retrievedBateau.toString());

		// Update
		em.getTransaction().begin();
		calendar.set(1966,12,12);
		marin1.setDateDeNaissance(calendar.getTime());
		em.getTransaction().commit();

		// Delete
		em.getTransaction().begin();
		em.remove(marin3);
		em.getTransaction().commit();



	}

}
