package org.name;

import java.sql.Date;
import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class MainMarin {

	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		Marin marin1 =  new Marin();
		marin1.setNom("Surcouf");
		marin1.setPrenom("Robert");
		marin1.setDateDeNaissance(Date.valueOf("1958-1-1"));

		Marin marin2 =  new Marin();
		marin2.setNom("Boris");
		marin2.setPrenom("VOUDOUNON");
		calendar.set(1974,2,15);
		marin2.setDateDeNaissance(calendar.getTime());

		Marin marin3 =  new Marin();
		marin3.setNom("AKenny");
		marin3.setPrenom("Rogers");
		calendar.set(1966,12,19);
		marin3.setDateDeNaissance(calendar.getTime());

		Marin marin4 =  new Marin();
		marin4.setNom("Fall");
		marin4.setPrenom("Ansou");
		calendar.set(1988,8,8);
		marin4.setDateDeNaissance(calendar.getTime());

		
		Bateau bateau1 = new Bateau();
		bateau1.setNom("Cargo");

		Bateau bateau2 = new Bateau();
		bateau2.setNom("Bombardier");

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-testExo1");

		EntityManager em = emf.createEntityManager();

		marin1.setBateau(bateau1);
		marin2.setBateau(bateau1);
		marin3.setBateau(bateau2);
		marin4.setBateau(bateau2);

		// Create
		em.getTransaction().begin();

		em.persist(marin1);
		em.persist(marin2);
		em.persist(marin3);

		em.getTransaction().commit();


		// Retrieve

		Marin retrievedMarin = em.find(Marin.class, 1L);
		System.out.println("Retieved Marin => " + retrievedMarin.getNom() + " " + retrievedMarin.getPrenom());

		// Update
		em.getTransaction().begin();
		calendar.set(1966,12,12);
		marin1.setDateDeNaissance(calendar.getTime());
		em.getTransaction().commit();

		// Delete
		em.getTransaction().begin();
		em.remove(marin3);
		em.getTransaction().commit(); 

	}

}
