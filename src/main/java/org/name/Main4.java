package org.name;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class Main4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub



		/** DECLARATIONS DES COMMUNES , MAIRES & DEPARTEMENTS & Adresses **/
		Commune commune1 = new Commune();			
		Commune commune2 = new Commune();	
		Commune commune3 = new Commune();

		Maire maire1 = new Maire();
		Maire maire2 = new Maire();
		Maire maire3 = new Maire();

		Departement departement1 = new Departement();
		
		Adresse adresse = new Adresse();
		Adresse adresse2 = new Adresse();

		/**AJOUTS**/
		commune1.setNomCommune("Aubervilliers");
		commune1.setCodePostal("93300");
		commune1.setMaire(maire1);
		commune1.setDepartement(departement1);

		//		System.out.println("***********TEST COMMUNE1***********");
		//		commune1.toString();

		commune2.setNomCommune("AulnaySousBois");
		commune2.setCodePostal("93600");
		commune2.setMaire(maire2);
		commune2.setDepartement(departement1);

		commune3.setNomCommune("Bagnolet");
		commune3.setCodePostal("93170");
		commune3.setMaire(maire3);
		commune3.setDepartement(departement1);


		/**MAIRE DE CHAQUE COMMUNES**/
		maire1.setNomMaire("Pascal Beaudet");
		maire1.setCommune(commune1);

		maire2.setNomMaire("BrunoBeschizza");		
		maire2.setCommune(commune2);

		//		System.out.println("***********TEST MAIRE1***********");
		//		System.out.println("\n LE MAIRE :" +maire2.getNomMaire().toString());

		maire3.setNomMaire("GTony Di");		
		maire3.setCommune(commune3);

		departement1.setNomDepartement("Seine-Saint-Denis");  
		departement1.AddCommune(commune1);
		departement1.AddCommune(commune2);
		departement1.AddCommune(commune3);	

		//Adresses
		adresse.setRue("11 Avenue leRoys des barres");
		//adresse2.setRue("11 Avenue leRoys des barres");
		
		adresse.setCommune(commune1.getNomCommune());
		//maire1.setAdresse(adresse);

		System.out.println("********TEST METHODE CLASSE ADRESSE*****");
		System.out.println("Adresse du maire (maison) :" + maire1.getNomMaire().toString());
		//System.out.println("Son Adresse:" + maire1.getAdresse().toString());
		
		
		adresse2.setRue("8 Place de Saint-denis");
		adresse2.setCommune(commune1.getNomCommune());
		//commune1.setAdresse(adresse2);
		//System.out.println("L'Adresse de la mairie:" + commune1.getAdresse().toString());
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-testExo1");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();
		em.persist(departement1);
		em.getTransaction().commit();

	}
}
