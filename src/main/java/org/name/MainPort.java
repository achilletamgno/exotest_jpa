package org.name;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class MainPort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Port port1 =  new Port();
		port1.setNom("port1");
		
		Port port2 =  new Port();
		port2.setNom("port2");

		Port port3 =  new Port();
		port3.setNom("port3");
		
		Port port4 =  new Port();
		port4.setNom("port4");


		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-testExo1");

		EntityManager em = emf.createEntityManager();

		Pays pays1 = new Pays();
		pays1.setNom("Cameroun");
		
		Pays pays2 = new Pays();
		pays2.setNom("Gabon");
		
		port1.setPays(pays1);
		port2.setPays(pays1);
		port3.setPays(pays2);
		port4.setPays(pays2);
		
		
		port2.setPays(pays2);
		
		// Create
		em.getTransaction().begin();

		em.persist(port1);
		em.persist(port2);
		em.persist(port3);
		em.persist(pays1);
		em.persist(pays2);

		em.getTransaction().commit();
		

	// Retrieve
	Port retrievedPort = em.find(Port.class, 1L);
	System.out.println("Retieved Port => " + retrievedPort.getNom());

	// Update
	em.getTransaction().begin();
	em.getTransaction().commit();

	// Delete
	    em.getTransaction().begin();
		em.remove(port3);
		em.getTransaction().commit(); 
	}


}
