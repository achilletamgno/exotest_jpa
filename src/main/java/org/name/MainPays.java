package org.name;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class MainPays {

	public static void main(String[] args) {
		
		Port port1 =  new Port();
		port1.setNom("port1");

		Port port2 =  new Port();
		port2.setNom("port2");

		Port port3 =  new Port();
		port3.setNom("port3");

		Port port4 =  new Port();
		port4.setNom("port4");


		EntityManagerFactory emf = Persistence.createEntityManagerFactory("jpa-testExo1");

		EntityManager em = emf.createEntityManager();

		Pays pays1 = new Pays();
		pays1.setNom("Cameroun");

		Pays pays2 = new Pays();
		pays2.setNom("Gabon");


		pays1.getListePort().add(port1);
		pays1.getListePort().add(port2);
		
		//pays2.getListePort().add(port3);
		//pays2.getListePort().add(port4);


		// Create
		em.getTransaction().begin();

		em.persist(pays1);
		em.persist(pays2);

		System.out.println("***********************************************************************************");

		for(Port m : pays1.getListePort()){
			System.out.println(m);
		}

		System.out.println("***********************************************************************************");
		for(Port m : pays2.getListePort()){
			System.out.println(m);
		}


		// Retrieve

		Pays retrievedPays = em.find(Pays.class, 1L);
		System.out.println("Retieved Port => " + retrievedPays);

		// Update
		//em.getTransaction().begin();
		em.getTransaction().commit();

		// Delete
		em.getTransaction().begin();
		em.remove(port3);
		em.getTransaction().commit(); 
	}

}
